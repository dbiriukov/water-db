# --- !Ups

CREATE TABLE house (
  id INT NOT NULL AUTO_INCREMENT,
  number VARCHAR(45) NOT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE tenant (
  id INT NOT NULL AUTO_INCREMENT,
  house INT NOT NULL,
  owner_name VARCHAR(45) NOT NULL,
  `comment` TEXT NULL,
  apartment VARCHAR(45) NOT NULL,
  PRIMARY KEY (id),
  CONSTRAINT fk_tenant_house
  FOREIGN KEY (house)
  REFERENCES house (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE payment (
  id INT NOT NULL AUTO_INCREMENT,
  tenant INT NOT NULL,
  `type` VARCHAR(10) NOT NULL,
  `name` VARCHAR(100) NOT NULL,
  amount decimal(8,2) NOT NULL DEFAULT 0,
  payment decimal(8,2) NOT NULL DEFAULT 0,
  `comment` TEXT NULL,
  PRIMARY KEY (id),
  CONSTRAINT fk_payment_tenant
  FOREIGN KEY (tenant)
  REFERENCES tenant (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;