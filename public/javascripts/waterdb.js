var app = angular.module('waterApp', ['ngResource', 'ngRoute'])
    .constant('apiUrl', 'http://localhost:9000/json');

app.factory('broadcastService', function($rootScope) {
    return {
        broadcastHouseChanged: function(id) { $rootScope.$broadcast('houseChangedTo', id); }
    };
});

app.controller('housesController', function($scope, $rootScope, houseService, broadcastService) {
    $scope.houses = [];
    $scope.currentHouse = null;

    $scope.activateHouse = function(house) {
        $scope.currentHouse = house.id;
        broadcastService.broadcastHouseChanged(house.id);
    };

    $scope.loadHouses = function () {
        houseService
            .getHouses()
            .then(function (houses) {
                $scope.houses = houses;
                if(houses.length > 0) {
                    $scope.activateHouse(houses[0]);
                }
            });
    };

    $scope.addForm = {};
    $scope.addForm.submit = function () {
        var house = {
            name: $scope.addForm.name
        };
        houseService
            .addNew(house)
            .then(function (house) {
                $scope.houses.push(house);
                $scope.addForm.name = '';
            });
    }
});

app.controller('tenantController', function($scope, tenantService) {
    $scope.tenantDtos = [];
    $scope.newTenant = {};
    $scope.showSaldoMap = {};

    var countSaldo = function(payments) {
        return _.reduce(payments, function(total, payment) {
            return total - payment.amount + payment.payment;
        }, 0);
    };

    $scope.$on('recalculateSaldo', function (event) {
        $scope.tenantDtos = _.map($scope.tenantDtos, function(tenant) {
            tenant.housingSaldo = _.option(tenant.payments.housing).fold(0, countSaldo);
            tenant.waterSaldo = _.option(tenant.payments.water).fold(0, countSaldo);

            return tenant;
        });
    });

    $scope.$on('houseChangedTo', function (event, house) {
        $scope.activeHouseId = house;
        tenantService
            .getTenants(house)
            .then(function (tenants) {
                _.each(tenants, function(p) {
                    if(!p.payments.housing) p.payments.housing = [];
                    if(!p.payments.water) p.payments.water = [];
                });

                $scope.tenantDtos = tenants;
                $scope.$emit('recalculateSaldo');
            });
    });

    $scope.showSaldo = function(tenantDto) {
        $scope.showSaldoMap[tenantDto.tenant.id] = _.option($scope.showSaldoMap[tenantDto.tenant.id])
            .fold(
                { water: true, housing: true },
                function(showSaldo) { return { housing: !showSaldo.housing, water: !showSaldo.water } });
    };

    $scope.save = function(tenantDto) {
        tenantService.save(tenantDto, $scope.activeHouseId);
    };

    $scope.create = function(tenantDto) {
        tenantService.save(tenantDto, $scope.activeHouseId).then(function(createdTenant) {
            var dto = {
                payments: {},
                tenant: createdTenant,
                housingSaldo: 0,
                waterSaldo: 0

            };
            $scope.newTenant = {};
            $scope.tenantDtos.push(dto);
        });

    };

    $scope.isPaymentsShown = function (saldoType, tenantDto) {
        return !!$scope.showSaldoMap[tenantDto.tenant.id] && !!$scope.showSaldoMap[tenantDto.tenant.id][saldoType];
    };

    $scope.makeEditable = function (tenant, field, state) {
        if(!tenant.editMap) {
            tenant.editMap = {};
        }

        tenant.editMap[field] = state;
    };

    $scope.clearAll = function(tenant) {
        tenant.editMap = {}
    };

    $scope.getPaymentCss = function(payment) {
        return {
            debt: payment < 0,
            payed: payment >= 0,
            bold: payment != 0
        };
    };
});

app.controller('paymentController', function($scope, paymentService) {
    $scope.newHousing = {};
    $scope.newWater = {};

    $scope.create = function(payment, tenantId, payments,  paymentType) {
        payment.ptype = paymentType;
        paymentService
            .save(payment, tenantId)
            .then(function(createdPayment) {
                $scope['new' + paymentType] = {};
                payments.push(createdPayment);

                $scope.$emit('recalculateSaldo');
            });
    };
});

app.service('houseService', function($http, $q, $resource) {

    var House = $resource('/json/house');

    return({
        getHouses: getHouses,
        addNew: addNew
    });

    function getHouses() {
        return (House.query().$promise.then( handleSuccess, handleError ));
    }

    function addNew(house) {
        return (House.save(house).$promise.then(handleSuccess, handleError ));
    }

    function handleError( response ) {
        if (!angular.isObject( response ) || !response.message) {
            return( $q.reject( 'An unknown error occurred.' ) );
        }

        return( $q.reject( response.message ) );
    }

    function handleSuccess( response ) {
        return( response );
    }
});

app.service('tenantService', function($http, $q, $resource) {
    var Tenants = $resource('/json/house/:house/tenants');

    return ({
        getTenants: getTenants,
        save: save
    });

    function getTenants(house) {
        return( Tenants.query({house: house}).$promise.then( handleSuccess, handleError ) );
    }

    function save(tenant, house) {
        var Tenant = $resource('/json/house/:house/tenants', {house: house});

        return (Tenant.save(tenant).$promise.then(handleSuccess, handleError()))
    }

    function handleError( response ) {
        if (!angular.isObject( response ) || !response.message) {
            return( $q.reject( 'An unknown error occurred.' ) );
        }

        return( $q.reject( response.message ) );
    }

    function handleSuccess( response ) {
        return( response );
    }
});

app.service('paymentService', function($http, $q, $resource) {
    var Payments = $resource('/json/tenant/:tenant/payments');

    return({
        save: save
    });

    function save(payment, tenant) {
        var Payment = $resource('/json/tenant/:tenant/payments', {tenant: tenant});

        return (Payment.save(payment).$promise.then(handleSuccess, handleError()))
    }

    function handleError( response ) {
        if (!angular.isObject( response ) || !response.message) {
            return( $q.reject( 'An unknown error occurred.' ) );
        }

        return( $q.reject( response.message ) );
    }

    function handleSuccess( response ) {
        return( response );
    }
});

app.filter('customCurrency', [ '$filter', '$locale', function(filter) {
    function isNumeric(value) {
        return (!isNaN(parseFloat(value)) && isFinite(value));
    }

    return function (inputNumber, currencySymbol, decimalSeparator, thousandsSeparator, decimalDigits) {
        if (!isNumeric(inputNumber)) {
            return inputNumber;
        }

        currencySymbol = _.option(currencySymbol).getOrElse("");
        decimalSeparator = _.option(decimalSeparator).getOrElse(",");
        thousandsSeparator = _.option(thousandsSeparator).getOrElse(" ");
        decimalDigits = _.option(decimalDigits).filter(isNumeric).getOrElse(2);

        if (decimalDigits < 0) {
            decimalDigits = 0;
        }

        var formattedNumber = filter('number')(inputNumber, decimalDigits);

        var numberParts = formattedNumber.split(".");

        numberParts[0] = numberParts[0].split(",").join(thousandsSeparator);

        var result = currencySymbol + numberParts[0];

        if (numberParts.length == 2) {
            result += decimalSeparator + numberParts[1];
        }

        return result;
    };
}]);

app.directive('ngEnter', function () {
    return function (scope, element, attrs) {
        element.bind('keydown keypress', function (event) {
            if(event.which === 13) {
                scope.$apply(function (){
                    scope.$eval(attrs.ngEnter);
                });

                event.preventDefault();
            }
        });
    };
});

app.directive('ngEsc', function () {
    return function (scope, element, attrs) {
        element.bind('keydown keypress', function (event) {
            if(event.which === 27) {
                scope.$apply(function (){
                    scope.$eval(attrs.ngEsc);
                });

                event.preventDefault();
            }
        });
    };
});

app.directive('focusMe', function($timeout, $parse) {
    return {
        link: function(scope, element, attrs) {
            var model = $parse(attrs.focusMe);
            scope.$watch(model, function(value) {
                if(value === true) {
                    $timeout(function() {
                        element[0].focus();
                    });
                }
            });
        }
    };
});

app.factory('Resource', [ '$resource', function( $resource ) {
    return function( url, params, methods ) {
        var defaults = {
            update: { method: 'put', isArray: false },
            create: { method: 'post' }
        };

        methods = angular.extend(defaults, methods);

        var resource = $resource(url, params, methods);

        resource.prototype.$save = function () {
            if (!this.id) {
                return this.$create();
            } else {
                return this.$update();
            }
        };

        return resource;
    };
}]);


