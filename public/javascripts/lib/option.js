_.mixin({
    option : function (value) {
        var obj = null;
        function isEmpty() { return value === undefined || value === null }
        function nonEmpty() { return !isEmpty() }
        function getField(field) {
            return value[field];
        }
        obj = {
            map: function (f) { return isEmpty() ? obj : _.option(f(value)) },
            filter: function(f) {
                if(isEmpty() || !f(value)) {
                    return _.option(null);
                } else {
                    return obj;
                }
            },
            fold: function(def, f) { return isEmpty() ? def : f(value) },
            getOrElse: function (n) { return isEmpty() ? n : value },
            orNull: function () { return isEmpty() ? null : value },
            isEmpty: isEmpty,
            nonEmpty: nonEmpty,
            maybe: function(field) { return isEmpty() ? obj : option(getField(field))}
        };

        return obj;
    }
});