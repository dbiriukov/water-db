package controllers

import model.entities.House
import model.facades.HouseFacade
import play.api.data._
import play.api.data.format.Formats._
import play.api.data.Forms._
import play.api.libs.json.Json._
import play.api.mvc._

object HouseService extends Controller {

  val addForm = Form(
    mapping(
      "id" -> optional(of[Long]),
      "name" -> nonEmptyText
    )
    ((id, name) => House(id, name))
    ((house: House) => Option(house.id, house.name))
  )

  implicit val houseFormat = writes[House]

  val facade = new HouseFacade

  def allHouses = Action {
    Ok(toJson(facade findAll()))
  }

  def findHouse(id: Long) = Action {
    Ok(toJson(facade find id))
  }

  def addHouse() = Action { implicit r =>
    val validatedForm = addForm.bindFromRequest()

    validatedForm.fold(
      errors => BadRequest,
      house => Ok(toJson(facade.add(house.name)))
    )
  }
}
