package controllers.dtos

import model.entities.{Payment, Tenant}

case class TenantWithPaymentsDto(tenant: Tenant, payments: Map[String, List[Payment]]) {

}
