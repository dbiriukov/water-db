package controllers

import model.entities.Payment
import model.facades.PaymentFacade
import play.api.data.format.Formats._
import play.api.data.Form
import play.api.data.Forms._
import play.api.libs.json.Json._
import play.api.mvc.{Action, Controller}

object PaymentService extends Controller {
  implicit val paymentFormat = writes[Payment]
  val facade = new PaymentFacade

  val addForm = Form(
    mapping(
      "id" -> optional(of[Long]),
      "ptype" -> nonEmptyText,
      "name" -> nonEmptyText,
      "amount" -> of[Double],
      "payment" -> of[Double],
      "comment" -> optional(text)
    )
      ((id, ptype, name, amount, payment, comment) => Payment(id, ptype, name, amount, payment, comment))
      ((p: Payment) => Option(p.id, p.ptype, p.name, p.amount, p.payment, p.comment))
  )

  def findByTenant(tenant: Long) = Action {
    val payments = facade.findByTenant(tenant)

    Ok(toJson(payments))
  }

  def addPayment(tenantId: Long) = Action { implicit r =>
    val validateForm = addForm.bindFromRequest()

    validateForm.fold(
      errors => BadRequest,
      payment => Ok(toJson(facade.addPayment(tenantId, payment)))
    )
  }


}
