package controllers

import model.entities.House
import model.facades.HouseFacade
import play.api.libs.json.Json._
import play.api.mvc._

object Application extends Controller {

  implicit val houseFormat = writes[House]

  val facade = new HouseFacade


  def index = Action {
    Ok(views.html.index("Your new application is ready.", facade.findAll()))
  }

}