package controllers

import controllers.dtos.TenantWithPaymentsDto
import model.entities.{Payment, Tenant}
import model.facades.{PaymentFacade, TenantFacade}
import play.api.data.Form
import play.api.data.Forms._
import play.api.data.format.Formats._
import play.api.libs.json.Json._
import play.api.mvc.{Action, Controller}

object TenantService extends Controller {
  implicit val tenantFormat = writes[Tenant]
  implicit val paymentFormat = writes[Payment]
  implicit val tenantWithPaymentsFormat = writes[TenantWithPaymentsDto]

  val facade = new TenantFacade
  val paymentFacade = new PaymentFacade

  val addForm = Form(
    mapping(
      "id" -> optional(of[Long]),
      "ownerName" -> nonEmptyText,
      "apartment" -> nonEmptyText,
      "comment" -> optional(of[String])
    )
      ((id, ownerName, apartment, comment) => Tenant(id, ownerName, apartment, comment))
      ((tenant: Tenant) => Option(tenant.id, tenant.ownerName, tenant.apartment, tenant.comment))
  )

  def findByHouse(house: Long) = Action {
    val tenants = facade.findByHouse(house)
    val tenantsWithPayments =
      tenants.map(t => TenantWithPaymentsDto(t, t.id.fold(List[Payment]())(paymentFacade.findByTenant).groupBy(p => p.ptype)))

    Ok(toJson(tenantsWithPayments))
  }

  def addTenant(houseId: Long) = Action { implicit r =>
    val validateForm = addForm.bindFromRequest()

    validateForm.fold(
      errors => BadRequest,
      tenant => Ok(toJson(facade.addTenant(houseId, tenant)))
    )
  }
}
