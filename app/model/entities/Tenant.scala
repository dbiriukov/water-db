package model.entities


case class Tenant(id: Option[Long], ownerName: String, apartment: String, comment: Option[String]) {
}