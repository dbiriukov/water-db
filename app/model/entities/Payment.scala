package model.entities

case class Payment(id: Option[Long], ptype: String, name: String, amount: Double, payment: Double, comment: Option[String]) {

}
