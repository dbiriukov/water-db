package model.facades

import model.entities.House

trait HouseFacadeLocal {
  def findAll(): List[House]
  def find(id: Long): House
  def add(name: String): House
}
