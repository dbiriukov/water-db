package model.facades

import model.entities.Payment

trait PaymentFacadeLocal {
  def findByTenant(tenant: Long): List[Payment]
  def findById(id: Long): Payment
  def addPayment(tenant: Long, payment: Payment): Payment
}
