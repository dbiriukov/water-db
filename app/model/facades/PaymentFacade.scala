package model.facades

import anorm.SqlParser._
import anorm._
import model.entities.Payment
import play.api.db.DB
import play.api.Play.current


class PaymentFacade extends PaymentFacadeLocal {
  val parser = {
    get[Option[Long]]("id")~
    get[String]("type")~
    get[String]("name")~
    get[Double]("amount")~
    get[Double]("payment")~
    get[Option[String]]("comment") map {
      case (id~ptype~name~amount~payment~comment) => Payment(id, ptype, name, amount, payment, comment)
    }
  }

  val findAllByTenant = SQL(
    """
      |SELECT id, tenant, type, name, amount, payment, comment FROM payment WHERE tenant = {tenant}
    """.stripMargin)

  val findById = SQL(
    """
      |SELECT id, tenant, type, name, amount, payment, comment FROM payment WHERE id = {id}
    """.stripMargin)

  val insertPayment = SQL(
    """
      |INSERT INTO payment (tenant, type, name, amount, payment, comment) VALUES({tenant}, {type}, {name}, {amount}, {payment}, {comment})
    """.stripMargin
  )

  def findByTenant(tenant: Long): List[Payment] = DB.withConnection(implicit c => {
    findAllByTenant.on("tenant" -> tenant).as(parser *)
  })

  def findById(id: Long): Payment = DB.withConnection(implicit c => {
    findById.on("id" -> id).as(parser single)
  })
  
  def addPayment(tenant: Long, payment: Payment): Payment = {
    val id: Option[Long] = DB.withConnection(implicit c => {
      insertPayment
        .on("tenant" -> tenant)
        .on("type" -> payment.ptype)
        .on("name" -> payment.name)
        .on("amount" -> payment.amount)
        .on("payment" -> payment.payment)
        .on("comment" -> payment.comment)
        .executeInsert()
    })

    findById(id.get)
  }
}
