package model.facades

import model.entities.Tenant

trait TenantFacadeLocal {
  def findByHouse(house: Long): List[Tenant]
  def addTenant(houseId: Long, tenant: Tenant): Tenant
}
