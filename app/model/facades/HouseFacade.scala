package model.facades

import anorm.SqlParser._
import anorm._
import model.entities.House
import play.api.db.DB
import play.api.Play.current


class HouseFacade extends HouseFacadeLocal {
  val selectAllHouses = SQL(
    """
      |SELECT id, number FROM house
    """.stripMargin)

  val selectHouseById = SQL(
    """
      |SELECT id, number FROM house WHERE id = {id}
    """.stripMargin)

  val insertHouse = SQL(
    """
      |INSERT INTO house (number) VALUES({number})
    """.stripMargin
  )

  val parser = {
    get[Option[Long]]("id")~
      get[String]("number") map {
      case (id~number) => House(id, number)
    }
  }

  override def findAll(): List[House] = DB.withConnection(implicit connection => selectAllHouses.as[List[House]](parser *))
  override def find(id: Long): House = DB.withConnection(implicit connection =>
    selectHouseById
      .on("id" -> id)
      .as[List[House]](parser *)
      .head)

  override def add(number: String): House = {
    val id: Option[Long] =  DB.withConnection(implicit c => {
      insertHouse.on("number" -> number).executeInsert()
    })
    find(id.get)
  }
}
