package model.facades

import anorm.SqlParser._
import anorm._
import model.entities.Tenant
import play.api.db.DB
import play.api.Play.current


class TenantFacade extends TenantFacadeLocal {
  val parser = {
      get[Option[Long]]("id")~
      get[String]("owner_name")~
      get[String]("apartment")~
      get[Option[String]]("comment") map {
      case (id~ownerName~apartment~comment) => Tenant(id, ownerName, apartment, comment)
    }
  }

  val findAllByHouse = SQL(
    """
      |SELECT id, owner_name, apartment, comment FROM tenant WHERE house = {house}
    """.stripMargin)

  val findById = SQL(
    """
      |SELECT id, owner_name, apartment, comment FROM tenant WHERE id = {id}
    """.stripMargin)


  val insertHouse = SQL(
    """
      |INSERT INTO tenant (house, owner_name, comment, apartment) VALUES({house}, {ownerName}, {comment}, {apartment})
    """.stripMargin
  )

  def findByHouse(house: Long): List[Tenant] = DB.withConnection(implicit c => {
    findAllByHouse.on("house" -> house).as(parser *)
  })

  def find(id: Long): Tenant = DB.withConnection(implicit c => {
    findById.on("id" -> id).as(parser single)
  })

  override def addTenant(houseId: Long, tenant: Tenant): Tenant = {
    val id: Option[Long] = DB.withConnection(implicit c => {
      insertHouse
        .on("house" -> houseId)
        .on("ownerName" -> tenant.ownerName)
        .on("comment" -> tenant.comment)
        .on("apartment" -> tenant.apartment)
        .executeInsert()
    })
    find(id.get)
  }
}
